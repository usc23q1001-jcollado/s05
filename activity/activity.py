
class Animal():
	def __init__(self):
		
		self._food = "foodies"

	def eat(self,food):
		print("Thy food")

	def make_sound(self):
		print("Grrrr")

	def call(self):
		print("Come")

class Doggo(Animal):
	def __init__(self,name,breed,age):
		super().__init__()
		self._name = name
		self.breed = breed
		self.age = age

	def call(self):
		print(f'Here {self._name}')
	def eat(self,food):
		self._food = food
		print(f'Eaten {self._food}')
	def make_sound(self):
		print("Bark! Woof! Arf!")

class Catto(Animal):
	def __init__(self,name,breed,age):
		super().__init__()
		self._name = name
		self.breed = breed
		self.age = age

	def call(self):
		print(f'{self._name} , Come on!')
	def eat(self,food):
		self._food = food
		print(f'Serve me{self._food}.')
	def make_sound(self):
		print("Miaow! Nyaw! Nyaaaa!")


dog1 = Doggo("Isis", "Dalmatian", 15)
cat1 = Catto("Ouss", "Persian",4)

dog1.eat("Steak")
dog1.make_sound()
dog1.call()
cat1.eat("Tuna")
cat1.make_sound()





	