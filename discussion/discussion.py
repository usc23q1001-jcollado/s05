# class SampleClass():
# 	def __init__(self,year):
# 		self.year = year

# 	def show_year(self):
# 		print(f'The year is: {self.year}')

# myObj = SampleClass(2020)

# print(myObj.year)
# myObj.show_year()
##FUNDA
##Encapsulation - data hiding
class Person():
	def __init__(self):
		self._name = "John Doe"
		self._age = 10

	def set_name(self,name):
		self._name = name 

	def get_name(self):
		print(f'Age of person: {self._name}')

	def set_age(self,age):
		self._age = age

	def get_age(self):
		print(f'Name of person: {self._age}')
# p1 = Person()
# print(p1.name)
# p1.get_name()
# p1.set_name("J Rizz")
# p1.get_name()


# p1.get_age()
# p1.set_age(30)
# p1.get_age()
##Inheritance

class Employee(Person):
	def __init__(self, employeeID):
		super().__init__()
		self._employeeID = employeeID

	def get_employeeID(self):
		print(f'The employee ID is: {self._employeeID}')
	def set_employeeID(self, employeeID):
		self._employeeID = employeeID
	def get_details(self):
		print(f"{self._employeeID} belongs to {self._name} ")
# emp1 = Employee("Emp.001")
# emp1.get_details()
# emp1.get_name()
# emp1.get_age()
# emp1.set_name("Jo Rizz")
# emp1.set_age(50)
# emp1.get_details()

class Student(Person):
	def __init__(self, student_no, course,year_level ):
		super().__init__()
		self._student_no = student_no
		self._course = course
		self._year_level = year_level

	def get_student_no(self):
		print(f"Student no of Student is{self._student_no}")

	def get_course(self):
		print(f"Course of student is {self.course}")

	def get_year_level(self):
		(print(f"The year level of student is{self._year_level}"))

	def ser_student_no(self,student_no):
		self._student_no = student_no

	def set_course (self,course):
		self._course = course

	def set_year_level(self, year_level):
		self.year_level = year_level


	def get_details(self):
		print(f"{self._name} is currently in year {self._year_level} taking up {self._course} .")

# student1 = Student("18100760","BSIT",4)

# student1.get_details()
	

##Polymorphism
class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print("Admin User")

class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print("Customer User")

def test_function(obj):
	obj.is_admin()
	obj.user_type()

user_admin = Admin()
user_customer = Customer()

test_function(user_admin)
test_function(user_customer)

class TeamLead():
	def occupatieam Lead')

	def hasAuton(self):
		print('Th(self):
		print(True)

class TeamMember():  
	def occupation(self):
		print('Team Member')

	def hasAuth(self):
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
	person.occupation()


class Zuitt():
	def tracks(self):
		print("We are currently offereing tracks(developer career, pi-shape career, and short courses)")

	def num_of_hours(self):
		print("Learn web development in 360 hours!")

class DeveloperCareer(Zuitt):
	def num_of_hours(self):
		print("Learn the basics of web development in 240 hours")

class PiShapedCareer(Zuitt):
	def num_of_hours(self):
		print("Learn skills for no code app dev in 140 hours")

class ShortCourses(Zuitt):
	def num_of_hours(self):
		print("Learn advance topics in web development in 20 hours")

course1 = DeveloperCareer()
course2 = PiShapedCareer()
course3 = ShortCourses()

course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()
##Abstraction

from abc import ABC, abstractclassmethod
class Polygon(ABC):
	def print_number_of_sides(self):

		pass

class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print("This polygon has 3 sides")



class Pentagon(Polygon):
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print("This polygon has 5 sides")

shape1 = Triangle()
shape2 = Pentagon()

shape1.print_number_of_sides()
shape2.print_number_of_sides()






